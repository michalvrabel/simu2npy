#include <Rtypes.h>
#include <TMath.h>
#include <iostream>

using namespace TMath;
using namespace std;

Int_t CalcUid(Int_t x, Int_t y, Int_t macrocellId) {
//    Int_t ec_col_n, ec_row_n, pmt_col_loc, pmt_row_loc, pmt_off_n;
//    Bool_t pmt_row_lt8, pmt_col_lt8;

//    //-------------
//
//    ec_col_n = CeilNint((x+1)/16); // error wrong division
//    ec_row_n = Abs(CeilNint((y+1)/16) - (3+1));
//
//    pmt_col_loc = x - ((ec_col_n-1)*16);
//    pmt_row_loc = (ec_row_n*16)-Abs(y-48);
//
//    pmt_col_lt8 = (pmt_col_loc<8);
//    pmt_row_lt8 = (pmt_row_loc<8);
//
//    if(!pmt_col_lt8) pmt_col_loc -= 8;
//    if(!pmt_row_lt8) pmt_row_loc -= 8;
//
//    pmt_off_n = (pmt_col_lt8*2) + pmt_row_lt8;
//
//    Int_t pid = (macrocellId-1)*2304 + (ec_row_n-1)*768 + (ec_col_n-1)*256 + pmt_off_n*64 + (pmt_col_loc)*8 + (pmt_row_loc) + 1;
//
//    //-------------

    Int_t ec_col_n, ec_row_n, /*shp_col_n, shp_row_n,*/ pmt_off_n,pmt_row_loc, pmt_col_loc;
    Short_t shp_row_lt8, shp_col_lt8;

	 //0.0625 = 3./48
	ec_col_n = Abs(CeilNint((x+1)/16.) /*- (3+1)*/);
	ec_row_n = Abs(CeilNint((y+1)/16.) - (3+1));

	/*shp_col_n*/pmt_col_loc = /*Abs((x)-48)*/ x - ((ec_col_n-1)*16);
	/*shp_row_n*/pmt_row_loc = (ec_row_n*16)-Abs(y-48);

	shp_col_lt8 = (/*shp_col_n*/pmt_col_loc<8);
	shp_row_lt8 = (/*shp_row_n*/pmt_row_loc<8);

	pmt_off_n = (shp_col_lt8*2) + shp_row_lt8; //( shp_col_lt8 * (1 + shp_row_lt8) ) + (shp_row_lt8);

	//pmt_row_loc = shp_row_n;
	//pmt_col_loc = shp_col_n;

	if(!shp_col_lt8) pmt_col_loc -= 8;
	if(!shp_row_lt8) pmt_row_loc -= 8;

	Int_t pid = (macrocellId-1)*2304 + (ec_row_n-1)*768 + (ec_col_n-1)*256 + pmt_off_n*64 + (pmt_col_loc)*8 + (pmt_row_loc) + 1;


//    if(printdebug) 
//      cout << "CalcUid: x=" << x << " y=" << y
//	  << " ec_col_n=" << ec_col_n << " s_row_n=" << ec_row_n
//	  //<< " shp_col_n=" << shp_col_n << " shp_row_n=" << shp_row_n
//	  << " pmt_off_n=" << pmt_off_n
//	  << " pmt_col_loc=" << pmt_col_loc << " pmt_row_loc=" << pmt_row_loc
//	  << endl;
    
//    cout << " ec_col_n: " << ec_col_n << " ec_row_n: " << ec_row_n << endl
//         << " shp_col_n: " << shp_col_n << " shp_row_n: " << shp_row_n << endl
//         << " pmt_col_lt8: " << pmt_col_lt8 << " pmt_row_lt8: " << pmt_row_lt8 << endl
//         << " pmt_off_n: " << pmt_off_n << " pmt_col_loc: " << pmt_col_loc  << " pmt_row_loc: " << pmt_row_loc << endl;
    
    return pid;
}



void calc_uid_test() {
    for (Int_t x=0; x<48; ++x) {
        for (Int_t y=0; y<48; ++y) {
            cout << x << " " << y << ": " << CalcUid(x,y,1) << endl;
        }
    }
    
    cout << "------------------------" << endl;
    
    
    for (Int_t y=0; y<48; ++y) {
        for (Int_t x=0; x<48; ++x) {
            cout << setw(5);
            cout << CalcUid(x,y,1);
        }
        cout << endl;
    }   
   
}
