#include <iostream>
typedef int Int_t;
using namespace std;

void CalcXY47(Int_t uid, Int_t * x47, Int_t * y47) {

	uid = uid - ((uid-1)/2304)*2304;

	Int_t row3 = (uid-1)/768;
		Int_t rowsec = uid - row3*768;

    Int_t col3 = (rowsec-1) >> 8; //-1
    Int_t colsec = rowsec - (col3 << 8);

	Int_t quadrant = (colsec-1) >> 6 ;// /64;
		Int_t quadsec = colsec - (quadrant << 6);
	Int_t octet = (quadsec-1) >> 3;
	Int_t octetNum = quadsec - (octet << 3);

	*x47 = (col3 << 4) + 8*(quadrant<2) + octet;
	*y47 = 47-((row3 << 4) + (((quadrant%2)+1) << 3) ) + octetNum;
}

Int_t CalcX47(Int_t uid) {

	uid = uid - ((uid-1)/2304)*2304;

	Int_t row3 = (uid-1)/768;
		Int_t rowsec = uid - row3*768; // seems to be faster than two added shifts
		//Int_t rowsec = uid - (row3 << 9) - (row3 << 8) ;

    Int_t col3 = (rowsec-1) >> 8; //-1
    Int_t colsec = rowsec - (col3 << 8);

	Int_t quadrant = (colsec-1) >> 6 ;// /64;
		Int_t quadsec = colsec - (quadrant << 6);
	Int_t octet = (quadsec-1) >> 3;
	Int_t octetNum = quadsec - (octet << 3);

	return (col3 << 4) + ((quadrant<2) << 3) + octet;
}

Int_t CalcY47(Int_t uid) {

	uid = uid - ((uid-1)/2304)*2304;

	cout << "uid=" << uid << endl;
	
	Int_t row3 = (uid-1)/768;
		Int_t rowsec = uid - row3*768;

	cout << "row3=" << row3 << endl;
		
    Int_t col3 = (rowsec-1) >> 8; //-1
		Int_t colsec = rowsec - (col3 << 8);
	
	cout << "col3=" << col3 << endl;

	Int_t quadrant = (colsec-1) >> 6 ;// /64;
		Int_t quadsec = colsec - (quadrant << 6);
		
	cout << "quadrant=" << quadrant << endl;
		
	Int_t octet = (quadsec-1) >> 3;
	
	cout << "octet=" << octet << endl;
	
	Int_t octetNum = quadsec - (octet << 3);

	cout << "octetNum=" << octetNum << endl;
	
	cout << "row3*16=" << (row3 << 4) << endl;
	
	return 47-((row3 << 4) + (((quadrant%2)+1) << 3) ) + octetNum;
}

void main() {

	cout << " x47=" << CalcX47(64519) << " y47=" << CalcY47(64519) << endl;//

}