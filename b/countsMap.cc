#include <vector>
#include <map>
#include <utility>  
#include <stdlib.h>
#include "TPolyLine3D.h"
#include <fstream>
#include <iostream>
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include "TFile.h"
#include "TTree.h"
#include "EEvent.hh"
#include "EFee.hh"
#include "EDetector.hh"
#include "ERunParameters.hh"
#include "EMacroCell.hh"

void countsMap(string simuout,string outTMP){
  gROOT->Reset();
  using namespace std;
  ifstream in1;
  ofstream OutFile(outTMP.c_str());
  in1.open(simuout.c_str());
  string nome1;
  Int_t numFile= 0;
  Int_t numEvent= 0;
  while(in1 >> nome1)
    {
      TFile  *fsimu = new TFile(nome1.c_str());
      TTree *etree = (TTree*)fsimu->Get("etree");
      EEvent *evs = new EEvent();
      evs->SetBranches(etree);
      Int_t nentries_simu = (Int_t)etree->GetEntries();
      //run over all events
      for(Int_t u=0;u<nentries_simu;u++){
        etree->GetEntry(u);
	cout<<"File "<<numFile<<endl;
	cout<<"Event "<<numEvent<<endl;
	if(evs->GetDetector ()->GetNumCells()>0)
	  OutFile<<"Event "<<numEvent<<endl;

	for(Int_t PDM_order=0;PDM_order<evs->GetDetector ()->GetNumCells();PDM_order++)
	  {
	    // calculates GTU maxima and minima
	    // calculates X and Y maxima and minima
	    Float_t minX=1e20,minY=1e20,maxX=-1e20,maxY=-1e20;
	    Int_t maxGTU = -1,minGTU=10000;
	    for(Int_t pix=0;pix<2304;pix++)
	      {
		Int_t ID_Pixel=2304 * (evs->GetDetector ()->GetMacroCell(PDM_order)->GetMCId() - 1 ) + pix + 1;
		Float_t valX = evs->GetRunPars ()->PixelCenter(ID_Pixel).X()+3000;
		Float_t valY = evs->GetRunPars ()->PixelCenter(ID_Pixel).Y()+3000;
		if(valX>maxX)
		  maxX = valX;
		if(valY>maxY)
		  maxY = valY;
		if(valX<minX)
		  minX = valX;
		if(valY<minY)
		  minY = valY;
	      }

	    for(Int_t t=0;t<evs->GetDetector ()->GetNumFee();t++)
	      {
		EFee* ph = evs->GetDetector()->GetFee(t);
		if(evs->GetDetector ()->GetMacroCell(PDM_order)->GetMCId()==evs->GetRunPars ()->MacroCell(ph->GetChUId()))
		  { 
		    if(ph->GetGtu()>maxGTU)
		      maxGTU = ph->GetGtu();
		    
		    if(ph->GetGtu()<minGTU)
		      minGTU =  ph->GetGtu();   
		  }
	      }
	    cout<<minGTU<<" "<<maxGTU<<endl;
	    //	    calculates PDM image
	    Int_t vectorSmall[4800][48] ={{0}};
	    for(Int_t t=0;t<evs->GetDetector ()->GetNumFee();t++)
	      {
		EFee* ph = evs->GetDetector()->GetFee(t);
		Float_t valX = evs->GetRunPars ()->PixelCenter(ph->GetChUId()).X()+3000;
		Float_t valY = evs->GetRunPars ()->PixelCenter(ph->GetChUId()).Y()+3000;
		Int_t pixNumX,pixNumY;
		if(evs->GetDetector ()->GetMacroCell(PDM_order)->GetMCId()==evs->GetRunPars ()->MacroCell(ph->GetChUId()))
		  {
		    for(Int_t PMT_X=0;PMT_X<6;PMT_X++)
		      {
			Float_t tmp_var = (valX-minX-(1.6692 * 2 * PMT_X))/2.9577;
			if(tmp_var<8*(PMT_X + 1) && tmp_var>=8*PMT_X)
			  {
			    pixNumX = tmp_var;
			  }
		      }

		    for(Int_t PMT_Y=0;PMT_Y<6;PMT_Y++)
		      {
			Float_t tmp_var = (valY-minY-(1.6692 * 2 * PMT_Y))/2.9577;
			if(tmp_var<8*(PMT_Y + 1) && tmp_var>=8*PMT_Y)
			  pixNumY = tmp_var;
		      }
		    if(ph->GetGtu()<99 && ph->GetGtu()>-1)
		      vectorSmall[ph->GetGtu()*48 + 48 - pixNumY -1][pixNumX] = ph->GetNumHits();
		  }
	      }
	    //prints PDM image
	    cout<<"PDM "<<evs->GetDetector ()->GetMacroCell(PDM_order)->GetMCId()<<endl;
	    OutFile<<"PDM "<<evs->GetDetector ()->GetMacroCell(PDM_order)->GetMCId()<<endl;
	    for(Int_t t=48*minGTU;t<48*maxGTU;t++)
	      {
		if(t<4800 && t>-1)
		  {
		    if(t%48==0)
		      OutFile<<"GTU "<<t/48<<endl;
		    
		    for(Int_t tt=0;tt<48;tt++)
		      OutFile<<vectorSmall[t][tt]<<" ";
		    
		    OutFile<<endl;
		  }
	      }
	  }
	numEvent++;
      }
            
      fsimu->~TFile();
      evs->~EEvent();

      numFile++;
    }
  OutFile.close();
}
