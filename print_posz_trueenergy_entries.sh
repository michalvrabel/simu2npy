#!/bin/bash
if [ $# -lt 1 ]; then
    echo "usage: ./print_posz_trueenergy_entries.sh SIMU_FILE [LAST_CHECKED_FILE]" >&2
    exit 1
fi
LAST_CHECKED_FILE=0
if [ $# -gt 1 ]; then
    LAST_CHECKED_FILE="$2"
fi

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

bash "$DIR/print_info.sh" --last "$LAST_CHECKED_FILE" "$1" | egrep 'EGeometry\.Pos\.Z|ETruth\.TrueEnergy|Number of entries'  | tee >(grep 'Number') | tail -n +2 | sort -u
