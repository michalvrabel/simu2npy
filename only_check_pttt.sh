#!/bin/bash
if [ $# -lt 1 ]; then
    echo "usage: ./only_check_pttt.sh simu_root_file [simu_root_file ...]" >&2
    exit 1
fi

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

python "$DIR/simu2npy.py" --no-hierarchy y -m 0 -C 0 -S 0 -c 0 -s 0 -e 0 -o /tmp $@
