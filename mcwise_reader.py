import numpy as np
import gzip
import os
import sys

if __name__ == "__main__" and '--save-visualization' in sys.argv: 
    index_save_vis = sys.argv.index('--save-visualization')
    if len(sys.argv) > index_save_vis+1 and str2bool(sys.argv[index_save_vis+1]):
        import matplotlib as mpl
        mpl.use('Agg')
        
import matplotlib.pyplot as plt

class McWiseFromFrancescoReader:
    file = None
    def __init__(self,file):
        self.file = file

    def read_event(self,num=0):
        frames = np.ones((128,48,48))*-1
        i = 0
        for l in self.file:
            frames[i//(48), i%(48)] = np.array([int(s.strip()) for s in l.split()])
            i += 1
            if i %(48*128) == 0:
                break
    
        return frames

    

def main(argv):
    
    counts = {1:None}

    with gzip.open("/home/vrabel/EUSO-Balloon/SPBDATA/some_files_node15/allpackets-SPBEUSO-ACQUISITION-20170429-110448-026.001--LONG-sqz/l1_tigger_kenji/trn_20170429-110448-026.001_pdm_asc.gz") as f:
        r = McWiseFromFrancescoReader(f)
        counts[1] = r.read_event(0)
        

    for mcid, data in counts.iteritems():
        #img = np.maximum.reduce(data)
        img = np.add.reduce(data)/len(data)
        #for gtu in range(len(data)):
        #img = data[gtu]
        fig, ax = plt.subplots(1)  #figsize=(8, 5), 
        ime = ax.imshow(img, interpolation = "nearest")
        ax.set_title("#{:d}: macrocell {:d} ".format(1, mcid))
        ax.set_xlabel('x [pixels]')
        ax.set_ylabel('y [pixels]')
        fig.colorbar(ime, label='max counts')
        plt.show()


    #with open("/tmp/ecwise.txt") as f:
    ##with gzip.open(os.path.join(args.output_dir,counts_ec_asc_npy_filename_format), 'wb') as f:
        #convert_macrocellwise_to_ecwise(counts, f, None)

if __name__ == "__main__":
    # execute only if run as a script
    main(sys.argv[1:])