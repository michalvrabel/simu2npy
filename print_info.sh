#!/bin/bash
if [ $# -lt 1 ]; then
	echo "usage: ./print_info.sh simu_file [simu_file ...]" >&2
	exit 1
fi

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

python "$DIR/simu2npy.py" --no-hierarchy y -o /tmp -c n -s n -C n -S n -e n -m n --print-status n --print-info y "$@"

