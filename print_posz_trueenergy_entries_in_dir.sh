#!/bin/bash
if [ $# -lt 1 ]; then
    echo "usage: ./print_posz_trueenergy_entries_in_dir.sh DIR_WITH_SIMU_FILES LAST_CHECKED_FILE" >&2
    exit 1
fi
LAST_CHECKED_FILE=""
if [ $# -gt 1 ]; then
    LAST_CHECKED_FILE="$2"
fi

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

find "$1" -name '*.root' | xargs -n1 -I {} bash -c 'echo "{}"; bash '"$DIR"'/print_posz_trueenergy_entries.sh "{}" '"$LAST_CHECKED_FILE"'; echo'