import simu2npy
import sys
import os
import numpy as np
import gzip

def main(argv):
    
    parser = argparse.ArgumentParser(description='Convert npy arrays into EC asc format (single macrocell)')
    parser.add_argument('npy_file', nargs='+', help="Macrocell .npy ndarray (gtus,height,width)")
    parser.add_argument('-o', '--output-dir', default=".", help="Output directory")
    
    args = parser.parse_args(argv)
    
    counts_ec_asc_npy_filename_format = '{}_ec_asc.gz'
    
    for pathame in args.npy_file:
        data = np.load(pathname)
        with gzip.open(os.path.join(args.output_dir,counts_ec_asc_npy_filename_format.format(ev_num)), 'wb') as f:
            simu2npy.convert_macrocellwise_to_ecwise(data, f)
    
       
if __name__ == "__main__":
    # execute only if run as a script
    main(sys.argv[1:])