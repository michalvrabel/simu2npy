#!/usr/bin/python
import sys
import os
import numpy as np
import math
import argparse
import gzip
sys.argv.append( '-b' )    # '-b-'
import ROOT

from collections import OrderedDict

def get_str2bool_func(exc_type=Exception):
    def str2bool(v):
        if isinstance(v, (int,bool,float)) or v is None:
            return bool(v)
        if v.strip().lower() in ('yes', 'true', 't', 'y', '1'):
            return True
        if v.strip().lower() in ('no', 'false', 'f', 'n', '0'):
            return False
        else:
            raise exc_type('Boolean value expected.')
    return str2bool

str2bool = get_str2bool_func()
str2bool_argparse = get_str2bool_func(argparse.ArgumentTypeError)


esaf_loaded = False

def load_esaf(reload_esaf = False):
    global esaf_loaded
    if esaf_loaded and not reload_esaf:
        return
    
    ROOT.gROOT.Reset()

    ROOT.gSystem.Load("libPhysics");              # print("Physics, ");
    ROOT.gSystem.Load("libGeom");                 # print("Geom, ");
    ROOT.gSystem.Load("libMinuit");               # print("Minuit, ");
    #     ROOT.gSystem.Load("libGui");            # 
    ROOT.gSystem.Load("libThread");               # print("libThread, ");
    ROOT.gSystem.Load("libGed");                  # print("Ged.\n");

    ROOT.gSystem.Load("libbase.so");              # print("base, ");
    ROOT.gSystem.Load("libSimuEvent.so");         # print("SimuEvent, ");
    ROOT.gSystem.Load("libgui.so");               # print("gui, ");

    # Simu libs
    ROOT.gSystem.Load("libtools.so");             # print("tools, ");
    ROOT.gSystem.Load("libelectronics.so");       # print("electronics, ");
    #const char* load_g4 = gSystem->Getenv("G4INSTALL");
    ROOT.gSystem.Load("liboptics.so");            # print("optics, ");
    ROOT.gSystem.Load("libatmosphere.so");        # print("atmosphere, ");
    ROOT.gSystem.Load("libgenbase.so");           # print("genbase, ");
    ROOT.gSystem.Load("libshowers.so");           # print("showers, ");
    ROOT.gSystem.Load("liblightsources.so");      # print("lightsources, ");

    ROOT.gSystem.Load("libCore");                 # print("Core.");
    ROOT.gSystem.Load("libRint");                 # print("Rint.");

    #ROOT.gClassTable.Print()

    #sys.exit(0)

    #ROOT.gSystem.Load("libsimuframework.so");     print("simuframework, ");
    ROOT.gSystem.Load("liblowtran.so");           # print("lowtran, ");
    ROOT.gSystem.Load("libcfortran.so");          # print("cfortran, \n");

        #ROOT.gSystem.Load("libslast.so");             print("slast, ");}

    ROOT.gSystem.Load("libradiativetransfer.so"); # print("radiativetransfer, ");
    ROOT.gSystem.Load("liblighttoeuso.so");       # print("lighttoeuso, ");
    ROOT.gSystem.Load("libdettools.so");          # print("dettools, ");

    #if (!TString(load_g4).IsNull()) {
    #    gROOT->LoadMacro("g4libs.C");
    #    g4libs();
    #    ROOT.gSystem.Load("libG4Detector.so");        print("G4Detector, ");
    #    ROOT.gSystem.Load("libG4fresnellens");        print("G4fresnellens, ");
    #}

    # Reco libs
    ROOT.gSystem.Load("libRecoRootEvent.so");     # print("RecoRootEvent, ");
    ROOT.gSystem.Load("libRecoViewer.so");        # print("RecoViewer, ");
    ROOT.gSystem.Load("librecoframework.so");     # print("recoframework, ");
    ROOT.gSystem.Load("libevent.so");             # print("event, ");
    ROOT.gSystem.Load("libinput.so");             # print("input, ");
    ROOT.gSystem.Load("liboutput.so");            # print("output, ");
                                                  # 
    ROOT.gSystem.Load("libenergy.so");            # print("energy, ");
    ROOT.gSystem.Load("libfitting.so");           # print("fitting, ");
    ROOT.gSystem.Load("libclustering.so");        # print("clustering, \n");
    ROOT.gSystem.Load("libfootprint.so");         # print("footprint, ");
    ROOT.gSystem.Load("libmass.so");              # print("mass, ");
    ROOT.gSystem.Load("libmeteorites.so");        # print("meteorites, ");
    ROOT.gSystem.Load("libTLE.so");               # print("TLE, ");  #//hibiki
    ROOT.gSystem.Load("libneutrino.so");          # print("neutrino, ");
    ROOT.gSystem.Load("libprofile.so");           # print("profile, ");
    ROOT.gSystem.Load("libtruth.so");             # print("truth, ");
                                                  # 
    # Event View libs                             # 
    ROOT.gSystem.Load("libEventViewer.so");       # print("EventViewer.\n");
    
    esaf_loaded = True


class SimuEventReader:
    root_file = None
    etree = None
    ev = None
    
    allow_create_hierarchy_dicts = True
    allow_create_counts_ndarrays = True
    allow_create_signals_ndarrays = True
    first_entry = 0
    last_entry = -1
    
    parse_gtus_func = None
    
    _next_entry = -1
    _etree_is_chain = False
    
    _last_ttree = None
    
    def load_etree(self, pathnames, use_tchain=True):
        #f = ROOT.TFile("/home/vrabel/esaf_testing_data/aesra/Run_fRunNumber-1--seed_IsolatedPrimary-7235--seed-42345/EusoElectronics_NightGlowRateOnAxis-0.42/GeneratorLightToEuso_EnergyRangeMin-1.000e+20--GeneratorLightToEuso_EnergyRangeMax-1.000e+20/GeneratorLightToEuso_ThetaRangeMin-60.0--GeneratorLightToEuso_ThetaRangeMax-61.0/GeneratorLightToEuso_PhiRangeMin-0.0--GeneratorLightToEuso_PhiRangeMax-360.0/simu.root")
        #f = ROOT.TFile("/home/vrabel/EUSO-Balloon/SPBDATA/para.2017-07-05-15h45m34s.root")
        #/home/vrabel/esaf_testing_data/esaf_output/Simu/Angle_75/simu_fseed11274.root
        #/home/vrabel/esaf_testing_data/esaf_configs/francesco_simple.cfg
        
        pathnames_list = pathnames if isinstance(pathnames,list) else [pathnames]
        ev = ROOT.EEvent()  
        etree = None
        etree_is_chain = False
        
        if not use_tchain:
            root_file = ROOT.TFile(pathnames[0])
            if not root_file:
                raise Exception("Cannot open file")
            if root_file.IsZombie():
                raise Exception("File is zombie")
            etree = root_file.Get("etree")
            etree_is_chain = False
            if not etree:
                raise Exception("No etree")
        
            ev.SetBranches(etree)
            self.root_file = root_file
        else:
            etree = ROOT.TChain("etree","etree chain")
            etree_is_chain = True
            for pathname in pathnames:
                etree.Add(pathname);
            
            # load th first event
            etree.GetListOfBranches() # causes to open the first file in the chain
            if etree.GetUserInfo().First():
                raise Exception("User info of the chain is expected to be empty")
        
            self.check_reset_tree()
        self.etree = etree
        self._etree_is_chain = etree_is_chain
        self.ev = ev
    
    def check_reset_tree(self):
        if self._etree_is_chain and self._last_ttree is not self.etree.GetTree():
            self._last_ttree = self.etree.GetTree()
            new_user_info = self._last_ttree.GetUserInfo()
            self.etree.GetUserInfo().Clear()
            for obj in new_user_info:
                self.etree.GetUserInfo().Add(obj)
            self.ev.SetBranches(self.etree)
            return True
        return False
        
    def close_files(self):
        if self.root_file:
            self.root_file.Close() 
            self.root_file = None
        if self._etree_is_chain and self.etree:
            self.etree.Reset()
        self.etree = None
     
    def get_num_entries(self):
        if not self.ev or not self.etree:
            raise Exception("etree not loaded")
        return self.etree.GetEntries()
    
    def create_hierarchy_dicts(self, ev=None):
        if ev is None:
            ev = self.ev
        if not ev:
            raise Exception("event not ready")
        
        rpars = ev.GetRunPars()
        det = ev.GetDetector()

        gtus = {}               #EDetector > EElectronics::GetGtuStart   #EDetector > EElectronics::GetGtuEnd
        macrocell_ids = set()

        for feeIndex in range(0, det.GetNumFee()):
            dfee = det.GetFee(feeIndex)
            gtu = int(dfee.GetGtu())
            uid = int(dfee.GetChUId())
            hits = int(dfee.GetNumHits())
            signals = int(dfee.GetNumSignals())
            feid = int(dfee.GetFEId())
            triggered = dfee.HasTriggered() ## !!!!
            
            mcid = int(rpars.MacroCell(uid))
            ecid = int(rpars.ElementaryCell(uid))
            pmtid = int(rpars.Pmt(uid))
            
            macrocell_ids.add(mcid)
            
            if gtu not in gtus:
                gtus[gtu] = {}
            
            if mcid not in gtus[gtu]:
                gtus[gtu][mcid] = {}
            
            if ecid not in gtus[gtu][mcid]:
                gtus[gtu][mcid][ecid] = {}
                
            if pmtid not in gtus[gtu][mcid][ecid]:
                gtus[gtu][mcid][ecid][pmtid] = {}
                
            gtus[gtu][mcid][ecid][pmtid][uid] = (hits, signals)
        
        return gtus, macrocell_ids
     
    def __init__(self, pathnames, parse_gtus_func, first_entry=0, last_entry=-1, allow_save_counts=True, allow_save_signals=True):
        self.parse_gtus_func = parse_gtus_func
        self.allow_create_counts_ndarrays = allow_save_counts
        self.allow_create_signals_ndarrays = allow_save_signals
        self.first_entry = first_entry
        self.last_entry = last_entry
        load_esaf()
        self.load_etree(pathnames)
    
    def __del__(self):
        self.close_files()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close_files()
        
    def __iter__(self):
        #self._next_entry = 0
        self._next_entry = self.first_entry
        return self
    
    def next(self):
        if self.etree and self._next_entry < self.etree.GetEntries() and (self.last_entry<0 or self._next_entry<=self.last_entry):
            
            r = self.get_entry(self._next_entry)
            self._next_entry += 1
            return r
        else:
            raise StopIteration()
    
    def get_entry(self, entry_num):
        if self.etree and entry_num < self.etree.GetEntries():
            self.etree.GetEntry(entry_num)
            
            if self.check_reset_tree():
                self.etree.GetEntry(entry_num)
               
            gtus = None
            counts = None
            signals = None
            macrocell_ids = None
                        
            if (self.allow_create_hierarchy_dicts(self.ev) if callable(self.allow_create_hierarchy_dicts) else self.allow_create_hierarchy_dicts):
                gtus, macrocell_ids = self.create_hierarchy_dicts()
                
                if self.allow_create_counts_ndarrays or self.allow_create_signals_ndarrays:
                    counts, signals = self.parse_gtus_func(self.ev, gtus, macrocell_ids, self.allow_create_counts_ndarrays, self.allow_create_signals_ndarrays)
            
        return self.ev, gtus, macrocell_ids, counts, signals


def check_gtus_hierarchy(gtus, num_ecs_per_macrocell, num_pmts_per_ec, num_uids_per_pmt):
    something_read = False
    
    for gtu, macrocells in gtus.iteritems():
        for mcid, ecs in macrocells.iteritems():
            if len(ecs) != num_ecs_per_macrocell:
                raise Exception('Unexpected number of ECs')
            for ecid, pmts in ecs.iteritems():
                if len(pmts) != num_pmts_per_ec:
                    raise Exception('Unexcepted number of PMTs')
                for pmtid, uids in pmts.iteritems():
                    if len(uids) > num_uids_per_pmt:    # 0 counts might not be present
                        raise Exception('Unexcepted number of pixels')
                    something_read = True
                    break
                if something_read:
                    break
            if something_read:
                break
        if something_read:
            break
            
    if not something_read:
        raise Exception('No fee read')
    

def read_dragon_like_macrocell(ev, gtus, macrocell_ids, allow_save_counts=True, allow_save_signals=True):
    rpars = ev.GetRunPars()
    
    ecs_per_macrocell_height = 3
    ecs_per_macrocell_width = 3
    
    pmts_per_ec_height = 2
    pmts_per_ec_width = 2
    
    uids_per_pmt_height = rpars.GetPmtData().GetNumRows()
    uids_per_pmt_width = rpars.GetPmtData().GetNumCols()
    
    if len(gtus) < 1:
        return None, None
    
    check_gtus_hierarchy(gtus, ecs_per_macrocell_height*ecs_per_macrocell_width, pmts_per_ec_height*pmts_per_ec_width, uids_per_pmt_height*uids_per_pmt_width)
    
    first_gtu = min(gtus.keys())
    last_gtu = max(gtus.keys())
    
    #
    #print("read_dragon_like_macrocell")
    #print("first_gtu = {:d} ; last_gtu = {:d}".format(first_gtu, last_gtu))
    #
    
    counts = None
    signals = None
        
    if allow_save_counts:
        counts = {}
        for mcid in macrocell_ids:
            if (callable(allow_save_counts) and allow_save_counts(ev.GetHeader().GetNum(), mcid, ev)) or allow_save_counts:    
                counts[mcid] = np.zeros((last_gtu - first_gtu +1,
                                            ecs_per_macrocell_height * pmts_per_ec_height * uids_per_pmt_height, 
                                            ecs_per_macrocell_width  * pmts_per_ec_width  * uids_per_pmt_width), dtype=np.int32)
            #else:
                #counts[mcid] = None
    if allow_save_signals:
        signals = {}
        for mcid in macrocell_ids:
            if (callable(allow_save_signals) and allow_save_signals(ev.GetHeader().GetNum(), mcid, ev)) or allow_save_signals: 
                signals[mcid] = np.zeros((last_gtu - first_gtu +1,
                                            ecs_per_macrocell_height * pmts_per_ec_height * uids_per_pmt_height, 
                                            ecs_per_macrocell_width  * pmts_per_ec_width  * uids_per_pmt_width), dtype=np.int32)
    
    #gtus*2130
    
    for gtu, macrocells in gtus.iteritems():
        for mcid, ecs in macrocells.iteritems():
            for ecid, pmts in ecs.iteritems():
                ecnum = ecid-(mcid-1)*9
                for pmtid, uids in pmts.iteritems():
                    pmtnum = pmtid-(ecid-1)*4
                    for uid, counts_signals in uids.iteritems():
                        channel = rpars.GetPmtData().Channel(uid)  
                        col_w = rpars.GetPmtData().Col(channel) +1
                        col  = channel // 8
                        row_w = rpars.GetPmtData().Row(channel)
                        row  = channel % 8
                    
                        #print("\t\t\t"+"uid={}   counts={} | channel={} pixrow={} pixelcol={} pixelrow_w={} pixcol_w={}".format(uid, counts, channel, row, col, row_w, col_w))
            
                        ec_y = ((ecnum-1) //  3)
                        ec_x = ((ecnum-1) % 3)
                            
                        #pmt_y = 1-((pmtnum-1) %  2)
                        #pmt_x = 1-((pmtnum-1) // 2)
                        pmt_y_map = [1,1,0,0]
                        pmt_x_map = [1,0,0,1]
                        pmt_y = pmt_y_map[pmtnum-1]
                        pmt_x = pmt_x_map[pmtnum-1]
                        
                        pix_x = col + ec_x*16 + pmt_x*8 
                        pix_y = row + ec_y*16 + pmt_y*8
                        
                        if counts and mcid in counts and isinstance(counts[mcid], np.ndarray):
                            counts[mcid][gtu-first_gtu,pix_y,pix_x] = counts_signals[0]
                        if signals and mcid in signals and isinstance(signals[mcid], np.ndarray):
                            signals[mcid][gtu-first_gtu,pix_y,pix_x] = counts_signals[1]
    return counts, signals


def read_sidecut_like_macrocell(ev, gtus, macrocell_ids, allow_save_counts=True, allow_save_signals=True):
    rpars = ev.GetRunPars()
    
    ecs_per_macrocell_height = 3
    ecs_per_macrocell_width = 3
    
    pmts_per_ec_height = 2
    pmts_per_ec_width = 2
    
    uids_per_pmt_height = rpars.GetPmtData().GetNumRows()
    uids_per_pmt_width = rpars.GetPmtData().GetNumCols()
    
    uids_per_ec_height = uids_per_pmt_height*pmts_per_ec_height
    uids_per_ec_width = uids_per_pmt_width*pmts_per_ec_width
    
    ecs_per_macrocell = ecs_per_macrocell_height*ecs_per_macrocell_width
    pmts_per_ec = pmts_per_ec_height*pmts_per_ec_width
    uids_per_pmt = uids_per_pmt_height*uids_per_pmt_width
    
    uids_per_ec = uids_per_pmt * pmts_per_ec
    uids_per_macrocell = uids_per_ec * ecs_per_macrocell
    uids_per_ec_row = uids_per_ec * ecs_per_macrocell_width
    uids_per_ec_col = uids_per_ec * ecs_per_macrocell_height
    
    uids_per_macrocell_height = uids_per_pmt_height * pmts_per_ec_height * ecs_per_macrocell_height
    
    if len(gtus) < 1:
        return None, None
    
    check_gtus_hierarchy(gtus, ecs_per_macrocell, pmts_per_ec, uids_per_pmt)
    
    first_gtu = min(gtus.keys())
    last_gtu = max(gtus.keys())
    
    counts = None
    signals = None
    
    if allow_save_counts:
        counts = {}
        for mcid in macrocell_ids:
            if (callable(allow_save_counts) and allow_save_counts(ev.GetHeader().GetNum(), mcid, ev)) or allow_save_counts:
                counts[mcid] = np.zeros((last_gtu - first_gtu +1,
                                            ecs_per_macrocell_height * pmts_per_ec_height * uids_per_pmt_height, 
                                            ecs_per_macrocell_width  * pmts_per_ec_width  * uids_per_pmt_width), dtype=np.int32)
    if allow_save_signals:
        signals = {}
        for mcid in macrocell_ids:
            if (callable(allow_save_signals) and allow_save_signals(ev.GetHeader().GetNum(), mcid, ev)) or allow_save_signals: 
                signals[mcid] = np.zeros((last_gtu - first_gtu +1,
                                            ecs_per_macrocell_height * pmts_per_ec_height * uids_per_pmt_height, 
                                            ecs_per_macrocell_width  * pmts_per_ec_width  * uids_per_pmt_width), dtype=np.int32)
    
    for gtu, macrocells in gtus.iteritems():
        for mcid, ecs in macrocells.iteritems():
            for ecid, pmts in ecs.iteritems():
                #ecnum = ecid-(mcid-1)*9
                for pmtid, uids in pmts.iteritems():
                    #pmtnum = pmtid-(ecid-1)*4
                    for uid, counts_signals in uids.iteritems():
                        
                        uid = uid - ((uid-1) // uids_per_macrocell)*uids_per_macrocell;

                        row3 = (uid-1)//uids_per_ec_row;  # shouldn't be uids_per_ec_col used here?
                        rowsec = uid - row3*uids_per_ec_row;

                        col3 = (rowsec-1) / uids_per_ec
                        colsec = rowsec - (col3 * uids_per_ec);

                        quadrant = (colsec-1) // uids_per_pmt  ;
                        quadsec = colsec - (quadrant * uids_per_pmt);
                        octet = (quadsec-1) // uids_per_pmt_height; # shouldn't be uids_per_pmt_width used here?
                        octetNum = quadsec - (octet * uids_per_pmt_height);


                        pix_x = (col3 * uids_per_ec_height) + uids_per_pmt_width*(quadrant<2) + octet;                           
                        pix_y = (uids_per_macrocell_height-1)-((row3 * uids_per_ec_width) + (((quadrant%2)+1) * uids_per_pmt_height) ) + octetNum;
                        
                        if counts and mcid in counts and isinstance(counts[mcid], np.ndarray):
                            counts[mcid][gtu-first_gtu,pix_y,pix_x] = counts_signals[0]
                        if signals and mcid in signals and isinstance(signals[mcid], np.ndarray):
                            signals[mcid][gtu-first_gtu,pix_y,pix_x] = counts_signals[1]
    return counts, signals


def print_event_props(ev, gtus, out_file=sys.stdout):
    head = ev.GetHeader()
    det = ev.GetDetector()
    det_status = det.GetStatus()
    pttt = ev.GetPTTTrigger()
    truth = ev.GetTruth()
    shower = ev.GetShower()
    atmo = ev.GetAtmosphere()
    geom = ev.GetGeometry()
    elec = det.GetElectronics()
            
    prop_list = \
        [
            ('Num event frames', len(gtus) if gtus is not None else -1),
            
            ('EHeader.Num', head.GetNum()),
            ('EHeader.Run', head.GetRun()),
            ('EHeader.RunName', head.GetRunName()),
            ('EHeader.Random.Seed', head.GetRandom().GetSeed()),
            ('EHeader.Status', head.GetStatus()),

            ('EDetector.NumPhotons', det.GetNumPhotons()),
            ('EDetector.NumCellHits', det.GetNumCellHits()),
            ('EDetector.NumFee', det.GetNumFee()),
            ('EDetector.NumAFee', det.GetNumAFee()),
            ('EDetector.NumCells', det.GetNumCells()),
            ('EDetector.FirstTime', det.GetFirstTime()),
            ('EDetector.LastTime', det.GetLastTime()),
            ('EDetector.FirstGtuTime', det.GetFirstGtuTime()),

            ('EDetStatus.NumActivePixels', det_status.GetNumActivePixels()),
            ('EDetStatus.NumSignals', det_status.GetNumSignals()),
            ('EDetStatus.NumBkg', det_status.GetNumBkg()),
            ('EDetStatus.NumHits', det_status.GetNumHits()),
            ('EDetStatus.NumGtu', det_status.GetNumGtu()),
            ('EDetStatus.NumPhotons', det_status.GetNumPhotons()),
            #('EDetStatus.MaxHits', det_status.GetMaxHits()),
            #('EDetStatus.MaxBkg', det_status.GetMaxBkg()),
            #('EDetStatus.MaxSignals', det_status.GetMaxSignals()),

            ('EPTTTrigger.fNumTrigg', pttt.fNumTrigg),
            ('EPTTTrigger.PTTTrackNum', pttt.GetPTTTrackNum()),

            ('EPTTTrigger.EPTTTriggerSegment[{:d}].', (pttt.GetPTTTrackNum(), lambda i: [ 
                ('NumEvt', pttt.GetPTTTrack(i).GetNumEvt()), 
                ('MaxCount', pttt.GetPTTTrack(i).GetMaxCount()),
                ('MaxGtu', pttt.GetPTTTrack(i).GetMaxGtu()),
                ('MaxRow', pttt.GetPTTTrack(i).GetMaxRow()),
                ('MaxCol', pttt.GetPTTTrack(i).GetMaxCol()),
                ('MaxChipID', pttt.GetPTTTrack(i).GetMaxChipID()),
                ('PdmID', pttt.GetPTTTrack(i).GetPdmID()),
            ])),
                        
            ('ETruth.TrueEnergy', truth.GetTrueEnergy()),    #Float_t
            ('ETruth.TrueTheta', truth.GetTrueTheta()),    #Float_t
            ('ETruth.TrueThetaLocal', truth.GetTrueThetaLocal()),    #Float_t
            ('ETruth.TruePhi', truth.GetTruePhi()),    #Float_t
            ('ETruth.TruePhiLocal', truth.GetTruePhiLocal()),    #Float_t
            ('ETruth.TrueParticleName', truth.GetTrueParticleName()),    #const char*
            ('ETruth.TrueParticleCode', truth.GetTrueParticleCode()),    #Int_t
            ('ETruth.TrueInitPos.X', truth.GetTrueInitPos().X()),    #Float_t
            ('ETruth.TrueInitPos.Y', truth.GetTrueInitPos().Y()),    #Float_t
            ('ETruth.TrueInitPos.Z', truth.GetTrueInitPos().Z()),    #Float_t
            ('ETruth.TrueX1', truth.GetTrueX1()),    #Float_t
            ('ETruth.TrueEarthImpact.X', truth.GetTrueEarthImpact().X()),    #Float_t
            ('ETruth.TrueEarthImpact.Y', truth.GetTrueEarthImpact().Y()),    #Float_t
            ('ETruth.TrueEarthImpact.Z', truth.GetTrueEarthImpact().Z()),    #Float_t
            ('ETruth.TrueEarthAge', truth.GetTrueEarthAge()),    #Float_t
            ('ETruth.TrueShowerMaxPos.X', truth.GetTrueShowerMaxPos().X()),    #Float_t
            ('ETruth.TrueShowerMaxPos.Y', truth.GetTrueShowerMaxPos().Y()),    #Float_t
            ('ETruth.TrueShowerMaxPos.Z', truth.GetTrueShowerMaxPos().Z()),    #Float_t
            ('ETruth.TrueShowerXMax', truth.GetTrueShowerXMax()),    #Float_t
            ('ETruth.TrueTOAImpact.X', truth.GetTrueTOAImpact().X()),    #Float_t
            ('ETruth.TrueTOAImpact.Y', truth.GetTrueTOAImpact().Y()),    #Float_t
            ('ETruth.TrueTOAImpact.Z', truth.GetTrueTOAImpact().Z()),    #Float_t
            ('ETruth.TrueHclouds', truth.GetTrueHclouds()),    #Float_t
            ('ETruth.TrueCloudsthick', truth.GetTrueCloudsthick()),    #Float_t
            ('ETruth.TrueCloudsOD', truth.GetTrueCloudsOD()),    #Float_t
            ('ETruth.TrueLatitude', truth.GetTrueLatitude()),    #Float_t
            ('ETruth.TrueLongitude', truth.GetTrueLongitude()),    #Float_t
            ('ETruth.TrueDate', truth.GetTrueDate()),    #Float_t
            ('ETruth.TrueHGround', truth.GetTrueHGround()),    #Float_t
            ('ETruth.TrueGroundAlbedo', truth.GetTrueGroundAlbedo()),    #Float_t
            ('ETruth.TrueGroundSpecular', truth.GetTrueGroundSpecular()),    #Float_t

            ('EShower.NumSteps', shower.GetNumSteps()),    #Int_t
            ('EShower.Energy', shower.GetEnergy()),    #Float_t
            ('EShower.Theta', shower.GetTheta()),    #Float_t
            ('EShower.Phi', shower.GetPhi()),    #Float_t
            ('EShower.X1', shower.GetX1()),    #Float_t
            ('EShower.ElectrEthres', shower.GetElectrEthres()),    #Float_t
            ('EShower.Dir.X', shower.GetDir().X()),    #Float_t
            ('EShower.Dir.Y', shower.GetDir().Y()),    #Float_t
            ('EShower.Dir.Z', shower.GetDir().Z()),    #Float_t
            ('EShower.InitPos.X', shower.GetInitPos().X()),    #Float_t
            ('EShower.InitPos.Y', shower.GetInitPos().Y()),    #Float_t
            ('EShower.InitPos.Z', shower.GetInitPos().Z()),    #Float_t
            ('EShower.HitGround', shower.GetHitGround()),    #Bool_t
            ('EShower.EventRadius', shower.GetEventRadius()),    #Double_t
            ('EShower.EventExtension', shower.GetEventExtension()),    #Double_t
            ('EShower.Tstep', shower.GetTstep()),    #Double_t
            ('EShower.IsTLE', shower.GetIsTLE()),    #Int_t

            ('EAtmosphere.NumBunches', atmo.GetNumBunches()),    #Int_t
            ('EAtmosphere.NumSingles', atmo.GetNumSingles()),    #Int_t
            ('EAtmosphere.MaxScatOrder', atmo.GetMaxScatOrder()),    #Int_t

            ('EGeometry.Pos.X', geom.GetPos().X()),    #Float_t
            ('EGeometry.Pos.Y', geom.GetPos().Y()),    #Float_t
            ('EGeometry.Pos.Z', geom.GetPos().Z()),    #Float_t
            ('EGeometry.Radius', geom.GetRadius()),    #Float_t
            ('EGeometry.OpticsRadius', geom.GetOpticsRadius()),    #Float_t
                        #const TRotation& GetRotation() const { return fRotation; }

            ('EElectronics.NumActiveGtus', elec.GetNumActiveGtus()),    #Int_t
            ('EElectronics.GtuStart', elec.GetGtuStart()),    #Int_t
            ('EElectronics.GtuEnd', elec.GetGtuEnd()),    #Int_t
            ('EElectronics.NumCells', elec.GetNumCells()),    #Int_t
            ('EElectronics.TimeGtu0', elec.GetTimeGtu0()),    #Float_t
            ('EElectronics.TimeFirstGtu', elec.GetTimeFirstGtu()),    #Float_t
            ('EElectronics.TimeLastGtu', elec.GetTimeLastGtu()),    #Float_t
            ('EElectronics.GtuLength', elec.GetGtuLength()),    #Float_t
        ]
    
    for prop_name, prop_val in prop_list:
        if isinstance(prop_val,(list,tuple)) and len(prop_val) == 2 and callable(prop_val[1]):
            for i in range(int(prop_val[0])):
                sub_list = prop_val[1](i)
                for sub_prop_name, sub_prop_val in sub_list:
                    out_file.write("{}{}:\t{}\n".format(prop_name.format(i), sub_prop_name, sub_prop_val))
        else:
            out_file.write('{}:\t{}\n'.format(prop_name, prop_val))

def convert_macrocellwise_to_ecwise(macrocellwise, out_file=sys.stdout, rpars=None):
    ecs_per_macrocell_height = 3
    ecs_per_macrocell_width = 3
    
    pmts_per_ec_height = 2
    pmts_per_ec_width = 2
    
    uids_per_pmt_height = 8 if not rpars else rpars.GetPmtData().GetNumRows()
    uids_per_pmt_width = 8 if not rpars else rpars.GetPmtData().GetNumCols()
    
    uids_per_ec_height = uids_per_pmt_height*pmts_per_ec_height
    uids_per_ec_width = uids_per_pmt_width*pmts_per_ec_width
        
    for gtu in range(len(macrocellwise)):
        for rowEC in range(0,ecs_per_macrocell_height):      # (int rowEC=0;rowEC<3;rowEC++)#writes EC-wise format (Mario)
            for colEC in range(0,ecs_per_macrocell_width):    # (int colEC=0;colEC<3;colEC++)
                for row in range(0,uids_per_ec_height):       # (int row=0;row<16;row++)
                    for col in range(0, uids_per_ec_width):   #(int col=0;col<16;col++)
                        out_file.write("{:d} ".format(macrocellwise[gtu, rowEC*uids_per_ec_height+row, colEC*uids_per_ec_width+col]))
                    out_file.write("\n")


def check_allow_create_hierarchy_dict(ev, allow_save_counts_func, allow_save_signals_func):
    if not callable(allow_save_counts_func) and allow_save_counts_func:
        return True
    if not callable(allow_save_signals_func) and allow_save_signals_func:
        return True
    
    event_id = ev.GetHeader().GetNum()
    macrocell_ids = [ev.GetDetector().GetMacroCell(i).GetMCId() for i in range(ev.GetDetector().GetNumCells())]
    
    for mc_id in macrocell_ids:
        if callable(allow_save_counts_func) and allow_save_counts_func(event_id, mc_id):
            return True
        if callable(allow_save_signals_func) and allow_save_signals_func(event_id, mc_id):
            return True
        
    return False
    
def event_reading_required_quick_check__all(overwrite, expected_num_events, output_dir, info_filename_format, presumed_num_mc, 
                                            save_signals, save_counts, save_counts_ec_asc, save_visualization, visualize_counts, visualize_signals, 
                                            signals_npy_pathname_func, ec_asc_file_pathname_func, counts_npy_pathname_func,
                                            integrated_counts_img_pathname_func, integrated_signals_img_pathname_func,
                                            ret_true_on_num_fee_missing=True, num_fee_attr='EDetector.NumFee',
                                            events_list=None):
    #args.save_info  # unnecessary check
    if overwrite:
        return True
    for ev_num in range(expected_num_events):
        reading_required, num_fee = event_reading_required_quick_check(
            ev_num, output_dir, info_filename_format, presumed_num_mc, 
            save_signals, save_counts, save_counts_ec_asc, save_visualization, visualize_counts, visualize_signals, 
            signals_npy_pathname_func, ec_asc_file_pathname_func, counts_npy_pathname_func,
            integrated_counts_img_pathname_func, integrated_signals_img_pathname_func,
            ret_true_on_num_fee_missing, num_fee_attr, return_num_fee=True)
        if reading_required: 
            return True
        elif isinstance(events_list, list):
            events_list.append((ev_num,'-',num_fee,'-'))
            
    return False
       
def event_reading_required_quick_check(ev_num, output_dir, info_filename_format, presumed_num_mc, 
                                       save_signals, save_counts, save_counts_ec_asc, save_visualization, visualize_counts, visualize_signals, 
                                       signals_npy_pathname_func, ec_asc_file_pathname_func, counts_npy_pathname_func,
                                       integrated_counts_img_pathname_func, integrated_signals_img_pathname_func,
                                       ret_true_on_num_fee_missing=True, num_fee_attr='EDetector.NumFee', prop_name_val_separator=':\t',
                                       return_num_fee=False):
    def ret_value(val, num_fee):
        if return_num_fee:
            return val, num_fee
        else:
            return val
    
    num_fee = None

    info_file_pathname = os.path.join(output_dir, info_filename_format.format(ev_num))
    if not os.path.exists(info_file_pathname):
        return ret_value(True, num_fee)

    try:
        with open(info_file_pathname,'r') as f:
            prop_w_sep = num_fee_attr+prop_name_val_separator
            for line in f: # ends with \n
                if line.startswith(prop_w_sep):
                    num_fee = int(line[len(prop_w_sep):-1])
                    break
    except ValueError:
        pass

    if num_fee is None or num_fee < 0:
        if ret_true_on_num_fee_missing:
            return ret_value(True, num_fee)
        else:
            raise RuntimeError('Missing attributte {} in info file {}'.format(num_fee_attr,info_file_pathname))
    
    if num_fee > 0:
        for mcid in range(1,presumed_num_mc+1):
            if save_signals:
                signals_npy_pathname = signals_npy_pathname_func(ev_num, mcid)
                if not os.path.exists(signals_npy_pathname):
                    return ret_value(True, num_fee)
            if save_counts_ec_asc:
                ec_asc_file_pathname = ec_asc_file_pathname_func(ev_num, mcid)
                if not os.path.exists(ec_asc_file_pathname):
                    return ret_value(True, num_fee)
            if save_counts:
                counts_npy_pathname = counts_npy_pathname_func(ev_num, mcid)
                if not os.path.exists(counts_npy_pathname):
                    return ret_value(True, num_fee)
            if save_visualization and visualize_counts:
                integrated_counts_img_pathname = integrated_counts_img_pathname_func(ev_num, mcid)
                if not os.path.exists(integrated_counts_img_pathname):
                    return ret_value(True, num_fee)
            if save_visualization and visualize_signals:
                integrated_signals_img_pathname = integrated_signals_img_pathname_func(ev_num, mcid)
                if not os.path.exists(integrated_signals_img_pathname):
                    return ret_value(True, num_fee)

    return ret_value(False, num_fee)
    

def main(argv):
    
    parser = argparse.ArgumentParser(description='Convert simu events into npy arrays')
    parser.add_argument('simu_root_file', nargs='+', help="ESAF simu .root file")
    parser.add_argument('-o', '--output-dir', default=".", help="Output directory (default: '.')")
    parser.add_argument('-t', '--input-type', default="sidecut_like_macrocell", help="Input type, supported types: sidecut_like_macrocell (default), dragon_like_macrocell")
    parser.add_argument('-f', '--first', default=0, type=int, help="First TTree to be read (default: 0).")
    parser.add_argument('-l', '--last', default=-1, type=int, help="Last TTree to be read  (default: -1).")
    
    parser.add_argument("-c","--save-counts", type=str2bool_argparse, default=True, help="If this option is true, counts ndarrays are saved  (default: yes).")
    parser.add_argument("-s","--save-signals", type=str2bool_argparse, default=True, help="If this option is true, signals ndarrays are saved  (default: yes).")
    parser.add_argument("-e","--save-counts-ec-asc", type=str2bool_argparse, default=False, help="If this option is true, counts in EC-wise (mario) format are saved  (default: no).")
    parser.add_argument("-C","--visualize-counts", type=str2bool_argparse, default=False, help="If this option is true, matplotlib visualization of (max) integrated counts is created (default: no).")
    parser.add_argument("-S","--visualize-signals", type=str2bool_argparse, default=True, help="If this option is true, matplotlib visualization of (max) integrated signals is created (default: yes).")
    parser.add_argument("--save-visualization", type=str2bool_argparse, default=True, help="If this option is true, matplotlib visualization is saved instead of opening interactive window (default: yes).")
    parser.add_argument("-m","--save-info", type=str2bool_argparse, default=True, help="If not empty, some event properties are written into a text file (default: yes).")
    parser.add_argument("--print-info", type=str2bool_argparse, default=False, help="If this option is true, some event properties are written into stdout, log is set to stderr (default: no).")
    parser.add_argument("--print-info-num-entries", type=str2bool_argparse, default=True, help="If this option is true and print-info option is true, total number of entries in TChain is printed (default: yes).")
    parser.add_argument("--print-status", type=str2bool_argparse, default=True, help="If this option is true, status is printed to stderr (default: yes).")
    parser.add_argument("--check-pttt", type=str2bool_argparse, default=True, help="If this option is true and status is printed, then PTTT triggered events are marked with 'PTTT' (default: yes).")
    parser.add_argument("--check-fee", type=str2bool_argparse, default=True, help="If this option is true and status is printed, then events with FEE are marked with 'FEE' (default: yes).")
    parser.add_argument("--overwrite", type=str2bool_argparse, default=True, help="If this option is true, existing files are overwritten (default: yes).")
    
    parser.add_argument("--no-hierarchy", type=str2bool_argparse, default=False, help="If this option is true, FEE are not read - useful for quick info output.")
        
    parser.add_argument('-b', action='store_true', help="Ignored.")
    
    parser.add_argument("--quick-check", type=str2bool_argparse, default=False, help="If this option is true, existence of files is checked based on info file (reading num fee) and other required files. Presumption of macrocell count is required. (default: no).")
    
    parser.add_argument("--quick-check-num-mc", default=1, type=int, help="Presumption of macrocell count. (default: 1).")

    args = parser.parse_args(argv)
       
    parse_gtus_func = None
    if args.input_type == "sidecut_like_macrocell":
        parse_gtus_func = read_sidecut_like_macrocell
    elif args.input_type == "dragon_like_macrocell":
        parse_gtus_func = read_dragon_like_macrocell
    
    info_filename_format = 'ev_{:d}__info.txt'
    counts_npy_filename_format = 'ev_{:d}_mc_{:d}__counts.npy'
    signals_npy_filename_format = 'ev_{:d}_mc_{:d}__signals.npy'
    counts_ec_asc_npy_filename_format = 'ev_{:d}_mc_{:d}__counts_ec_asc.gz'
    
    integrated_counts_img_filename_format = 'ev_{:d}_mc_{:d}__max_integrated_counts.png'
    integrated_signals_img_filename_format = 'ev_{:d}_mc_{:d}__integrated_signals.png'
    
    ec_asc_file_pathname_func = lambda ev_num, mcid: os.path.join(args.output_dir,counts_ec_asc_npy_filename_format.format(ev_num, mcid))
    counts_npy_pathname_func = lambda ev_num, mcid: os.path.join(args.output_dir, counts_npy_filename_format.format(ev_num, mcid))
    signals_npy_pathname_func = lambda ev_num, mcid: os.path.join(args.output_dir, signals_npy_filename_format.format(ev_num, mcid))
    integrated_counts_img_pathname_func = lambda ev_num, mcid: os.path.join(args.output_dir,integrated_counts_img_filename_format.format(ev_num, mcid))
    integrated_signals_img_pathname_func = lambda ev_num, mcid: os.path.join(args.output_dir,integrated_signals_img_filename_format.format(ev_num, mcid))
    
    allow_save_counts_func = lambda ev_num, mcid, ev=None: \
                        (args.save_counts and (args.overwrite or not os.path.exists(counts_npy_pathname_func(ev_num, mcid)))) or \
                        (args.visualize_counts and (not args.save_visualization or args.overwrite or not os.path.exists(integrated_counts_img_pathname_func(ev_num, mcid)))) or \
                        (args.save_counts_ec_asc and (args.overwrite or not os.path.exists(ec_asc_file_pathname_func(ev_num, mcid))))
    allow_save_signals_func = lambda ev_num, mcid, ev=None: \
                         (args.save_signals and (args.overwrite or not os.path.exists(signals_npy_pathname_func(ev_num, mcid)))) or \
                         (args.visualize_signals and (not args.save_visualization or args.overwrite or not os.path.exists(integrated_signals_img_pathname_func(ev_num, mcid))))
    
    check_allow_create_hierarchy_dict_func = lambda ev: check_allow_create_hierarchy_dict(ev, allow_save_counts_func, allow_save_signals_func)
    
    logfile = sys.stderr if args.print_info else sys.stdout
    log_msg_header_format = "Event #{:<4d} "
    
    if os.path.isfile(args.output_dir):
        raise Exception('Output dir is file')
    if not os.path.exists(args.output_dir):
        os.makedirs(args.output_dir)
    
    load_esaf()
    
    num_with_fee = 0 if args.check_fee else -1
    num_pttt_triggered = 0 if args.check_pttt else -1
    
    if args.visualize_counts or args.visualize_signals:
        if __name__ == "__main__" and '--save-visualization' in sys.argv: 
            index_save_vis = sys.argv.index('--save-visualization')
            if len(sys.argv) > index_save_vis+1 and str2bool(sys.argv[index_save_vis+1]):
                import matplotlib as mpl
                mpl.use('Agg')
                
        import matplotlib.pyplot as plt

    # "/home/vrabel/EUSO-Balloon/SPBDATA/para.2017-07-05-15h45m34s.root".2017-07-05-15h45m34s.root"
    with SimuEventReader(args.simu_root_file, parse_gtus_func, 
                         first_entry=args.first,
                         last_entry=args.last,
                         allow_save_counts=allow_save_counts_func,
                         allow_save_signals=allow_save_signals_func
                         ) as simu_reader:

        if args.no_hierarchy:
            simu_reader.allow_create_hierarchy_dicts = not args.no_hierarchy
        elif not args.overwrite:
            simu_reader.allow_create_hierarchy_dicts = check_allow_create_hierarchy_dict_func
        
        num_entries = simu_reader.get_num_entries()
        
        if args.print_info and args.print_info_num_entries:
            print("Number of entries:\t{:d}".format(num_entries))
        if args.print_status:
            logfile.write("Number of entries:\t{:d}\n".format(num_entries).encode())
            logfile.flush()
        
        if args.quick_check:
            events_list = []
            if not event_reading_required_quick_check__all(
                    args.overwrite, num_entries, args.output_dir, info_filename_format, args.quick_check_num_mc, 
                    args.save_signals, args.save_counts, args.save_counts_ec_asc, args.save_visualization, args.visualize_counts, args.visualize_signals, 
                    signals_npy_pathname_func, ec_asc_file_pathname_func, counts_npy_pathname_func,
                    integrated_counts_img_pathname_func, integrated_signals_img_pathname_func, events_list=events_list):
                if args.print_status: 
                    for ev_num, num_pttt, num_fee, status_str in events_list:
                        logfile.write((log_msg_header_format + 'PTTT {:<3} FEE {:<7d} {}\n').format(ev_num, num_pttt, num_fee, status_str))
                    logfile.flush()
                return 0
        
        for ev, gtus, macrocell_ids, counts, signals in simu_reader:
            ev_num = ev.GetHeader().GetNum()
            
            if args.print_status: 
                logfile.write(log_msg_header_format.format(ev_num)); logfile.flush()
                if args.check_pttt:
                    if ev.GetPTTTrigger().fNumTrigg > 0:  # EPTTTrigger.PTTTrackNum might be more correct
                        logfile.write('PTTT {:<3d} '.format(ev.GetPTTTrigger().fNumTrigg))
                        num_pttt_triggered += 1
                    else:
                        logfile.write('         ')
                if args.check_fee:
                    if ev.GetDetector().GetNumFee() > 0:  # EPTTTrigger.PTTTrackNum might be more correct
                        logfile.write('FEE {:<7d} '.format(ev.GetDetector().GetNumFee()))
                        num_with_fee += 1
                    else:
                        logfile.write('            ')
            
            if args.print_info:
                print_event_props(ev, gtus, sys.stdout)
                 
            if args.save_info:
                info_file_pathname = os.path.join(args.output_dir, info_filename_format.format(ev_num))
                info_file_pathname_exists = os.path.exists(info_file_pathname)
                if args.overwrite or not info_file_pathname_exists:
                    with open(info_file_pathname,"w") as f:
                        print_event_props(ev, gtus, f)
                        if args.print_status: logfile.write("m"); logfile.flush()
                elif info_file_pathname_exists:
                    if args.print_status: logfile.write('-'); logfile.flush()
                    # if logfile != sys.stderr: sys.stderr.write('Info file {} already exists, skipping\n'.format(info_file_pathname))
            elif args.print_status: logfile.write(" "); logfile.flush()
            
            if signals and args.save_signals:
                something_saved = False
                error_happend = False
                for mcid, data in signals.iteritems():
                    signals_npy_pathname = signals_npy_pathname_func(ev_num, mcid)
                    signals_npy_pathname_exists = os.path.exists(signals_npy_pathname)
                    if data is not None:
                        if args.overwrite or not signals_npy_pathname_exists:
                            np.save(signals_npy_pathname, data, False)
                            something_saved = True
                    else:
                        error_happend = True
                    #elif args.print_status: logfile.write('E'); logfile.flush()
                if args.print_status: 
                    if error_happend:
                        logfile.write("E"); 
                    elif something_saved:
                        logfile.write("s"); 
                    else:
                        logfile.write('-');
                    logfile.flush()
                    
            elif args.print_status: logfile.write(" "); logfile.flush()
            
            if counts:
                if args.save_counts_ec_asc:
                    something_saved = False
                    error_happend = False
                    for mcid, data in counts.iteritems():
                        ec_asc_file_pathname = ec_asc_file_pathname_func(ev_num, mcid)
                        ec_asc_file_pathname_exists = os.path.exists(ec_asc_file_pathname)
                        if data is not None: 
                            if args.overwrite or not ec_asc_file_pathname_exists:
                                with gzip.open(ec_asc_file_pathname, 'wb') as f:
                                    convert_macrocellwise_to_ecwise(data, f, ev.GetRunPars())
                                something_saved = True
                        else:
                            error_happend = True
                    if args.print_status: 
                        if error_happend:
                            logfile.write("E"); 
                        elif something_saved:
                            logfile.write("e"); 
                        else:
                            logfile.write('-');
                        logfile.flush()
                elif args.print_status: logfile.write(" "); logfile.flush()
                        
                if args.save_counts:
                    something_saved = False
                    error_happend = False
                    for mcid, data in counts.iteritems():
                        counts_npy_pathname = counts_npy_pathname_func(ev_num, mcid)
                        counts_npy_pathname_exists = os.path.exists(counts_npy_pathname)
                        if data is not None:
                            if args.overwrite or not counts_npy_pathname_exists:
                                np.save(counts_npy_pathname, data, False)
                                something_saved = True
                        else:
                            error_happend = True
                    if args.print_status:
                        if error_happend:
                            logfile.write("E"); 
                        elif something_saved:
                            logfile.write("c");
                        else:
                            logfile.write('-');
                        logfile.flush()
                elif args.print_status: logfile.write(" "); logfile.flush()
                    
                for mcid, data in counts.iteritems():
                    #for gtu in range(len(data)):
                    #img = data[gtu]
                    integrated_counts_img_pathname = integrated_counts_img_pathname_func(ev_num, mcid)
                    if data is not None and args.visualize_counts and (not args.save_visualization or args.overwrite or not os.path.exists(integrated_counts_img_pathname)):
                        img = np.maximum.reduce(data)
                        fig, ax = plt.subplots(1)  #figsize=(8, 5), 
                        ime = ax.imshow(img, interpolation = "nearest")
                        ax.set_title("#{:d}: macrocell {:d} ".format(ev_num, mcid))
                        ax.set_xlabel('x [pixels]')
                        ax.set_ylabel('y [pixels]')
                        fig.colorbar(ime, label='max counts within {:d} GTU'.format(len(data)))
                        if args.print_status: logfile.write("C"); logfile.flush()
                        if args.save_visualization:
                            fig.savefig(integrated_counts_img_pathname)
                        else:
                            print("plt.show()")
                            plt.show()
                        plt.close('all')
                    elif args.print_status: logfile.write(" "); logfile.flush()
            
            if signals:
                for mcid, data in signals.iteritems():
                    integrated_signals_img_pathname = integrated_signals_img_pathname_func(ev_num, mcid)
                    if data is not None and args.visualize_signals and (not args.save_visualization or args.overwrite or not os.path.exists(integrated_signals_img_pathname)):
                        img = np.add.reduce(data)
                        fig, ax = plt.subplots(1)  #figsize=(8, 5), 
                        ime = ax.imshow(img, interpolation = "nearest")
                        ax.set_title("#{:d}: macrocell {:d} ".format(ev_num, mcid))
                        ax.set_xlabel('x [pixels]')
                        ax.set_ylabel('y [pixels]')
                        fig.colorbar(ime, label='integrated signals within {:d} GTU'.format(len(data)))
                        if args.print_status: logfile.write("S"); logfile.flush()
                        if args.save_visualization:
                            fig.savefig(integrated_signals_img_pathname)
                        else:
                            print("plt.show()")
                            plt.show()
                        plt.close('all')
                    elif args.print_status: logfile.write(" "); logfile.flush()
                    
            if args.print_status:
                logfile.write('\n')
    
    if args.print_status: 
        logfile.write('\nOutput saved in "{}".\n{} events have EDetector.NumFee > 0.\n{} events have EPTTTrigger.fNumTrigg > 0.\n'.format(args.output_dir, num_with_fee, num_pttt_triggered)) 
        logfile.flush()
        
    return 0
    
    
if __name__ == "__main__":
    # execute only if run as a script
    main(sys.argv[1:])
